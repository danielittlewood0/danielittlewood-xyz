# -*- MakeFile -*-

DOMAIN  = https://www.danielittlewood.xyz

# The site will be built into the `public` directory, which is intended to be
# uploaded verbatim. (TAR meaning TARGET below)

# The site is built from the following kinds of files:
# * Plain CSS stylesheets, which are copied verbatim from source.
# * Notes written in Markdown, which are converted to HTML fragments using
#   pandoc (https://pandoc.org/).
FONTS     = public/Libre-Baskerville public/Inconsolata
STYLES    = $(wildcard src/**/*.css src/*.css) public/highlighting.css
FAVICON   = src/d.svg
HTACCESS  = src/.htaccess
HTML      = $(wildcard src/**/*.html)
JS        = $(wildcard src/**/*.js)
IMGS      = $(wildcard src/**/*.png)
COPY_SRC  = $(STYLES) $(FAVICON) $(HTACCESS) $(FONTS) $(HTML) $(JS) $(IMGS)
COPY_TAR  = $(COPY_SRC:src/%=public/%)
SITEMAP   = public/sitemap.xml
FILTER_SRC= $(wildcard filters/*.hs)
FILTERS   = $(FILTER_SRC:%.hs=%)
NOTES_SRC = $(wildcard src/*.md) $(wildcard src/**/*.md)
NOTES_TAR = $(NOTES_SRC:src/%.md=public/%.html)

# Required for tasks without dependencies. See the manual for more info.
.PHONY: all clean

# Run `make all` to update any files (or to create them for
# the first time), and `make clean` to clear the `public` directory.
all: $(NOTES_TAR) $(COPY_TAR) $(SITEMAP) $(FILTERS) public/Libre-Baskerville/LICENSE.txt public/Libre-Baskerville/REGULAR.ttf public/Libre-Baskerville/BOLD.ttf public/Libre-Baskerville/ITALIC.ttf public/Inconsolata/LICENSE.txt public/Inconsolata/REGULAR.ttf public/Inconsolata/BOLD.ttf

clean:
	rm -rf public/*
	find filters ! -name '*.hs' -type f -exec rm {} +

public/Libre-Baskerville:
	mkdir public/Libre-Baskerville

# The pipe explains to `make` that the source (public/Libre-Baskerville) is an
# "order-only prerequisite". This means `make` will ignore the modification
# date of the source when deciding whether to re-create the target or not.
# For further details see
# https://www.gnu.org/software/make/manual/html_node/Prerequisite-Types.html
public/Libre-Baskerville/LICENSE.txt: | public/Libre-Baskerville
	wget -O $@ https://github.com/impallari/Libre-Baskerville/raw/master/OFL.txt

public/Libre-Baskerville/REGULAR.ttf: | public/Libre-Baskerville
	wget -O $@ https://github.com/impallari/Libre-Baskerville/raw/master/legacy/version-1.000%20-%20Initial%20Release/LibreBaskerville-Regular.ttf

public/Libre-Baskerville/BOLD.ttf: | public/Libre-Baskerville
	wget -O $@ https://github.com/impallari/Libre-Baskerville/raw/master/legacy/version-1.000%20-%20Initial%20Release/LibreBaskerville-Bold.ttf

public/Libre-Baskerville/ITALIC.ttf: | public/Libre-Baskerville
	wget -O $@ https://github.com/impallari/Libre-Baskerville/raw/master/legacy/version-1.000%20-%20Initial%20Release/LibreBaskerville-Italic.ttf

public/Inconsolata:
	mkdir public/Inconsolata

public/Inconsolata/LICENSE.txt: | public/Inconsolata
	wget -O $@ https://github.com/googlefonts/Inconsolata/raw/main/OFL.txt

public/Inconsolata/REGULAR.ttf: | public/Inconsolata
	wget -O $@ https://github.com/googlefonts/Inconsolata/raw/main/fonts/ttf/Inconsolata-SemiCondensedSemiBold.ttf

public/Inconsolata/BOLD.ttf: | public/Inconsolata
	wget -O $@ https://github.com/googlefonts/Inconsolata/raw/main/fonts/ttf/Inconsolata-Bold.ttf


# Official documentation at https://pandoc.org/filters.html
# Each filter I wrote is documented with source code comments
filters/%: filters/%.hs
	ghc --make $< -o $@

FILTER_OPTS = $(FILTERS:%=--filter=%)
public/%.html: src/%.md $(FILTERS) danielittlewood-template.html
	mkdir -p public/notes
	pandoc \
		--template="danielittlewood-template.html" \
		--toc-depth=6 \
		$(FILTER_OPTS) \
		-V lang=en \
		-M author="admin@danielittlewood.xyz" \
		-V css="/main.css" \
		-V css="/highlighting.css" \
		-V favicon="/d.svg" \
		-V toc-title="In this page" \
		--toc -so $@ $<

public/highlighting.css: $(NOTES_SRC)
	cat $(NOTES_SRC) | pandoc --template="highlighting.template" -o public/highlighting.css -t html

# For some reason I decided that building this with a shell script was a good
# idea. See the script itself for more details.
public/sitemap.xml:
	DOMAIN=$(DOMAIN) ./sitemap_generator.sh >| public/sitemap.xml

# This is a fallback. Any target NOT matching a previous rule will just be copied over.
# Note that the target must be in COPY_TAR for this to work
public/%: src/%
	mkdir -p public/simpsons
	cp $< $@
