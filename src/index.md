---
title: danielittlewood.xyz
---

Welcome to my site, where I blog about software and open culture.
See below for articles I've written, and browse [the documentation] or [the
source] if you want to learn how the site works.

### Notes about software freedom

* [What is Free Software?](./notes/what-is-free-software.html)
* [Legal Drag, or: how technology facilitates the stealing of legal rights](./notes/legal-drag.html)

## Notes about technology generally

::: details
[DeGoogle: Leaving Gmail]

My personal tips for how to get off Gmail for good: setting up your domain,
email forwarding, IMAP and SMTP, and migrating your data over.
:::

::: details
[Everything in its right place](./notes/everything-in-its-right-place)

Ideas for how to keep your computer organised - software for tagging files, and
how to manage the resulting complexity.
:::

::: details
[Migrating to NixOS]

An article documenting how I installed NixOS in 2023. I was not completely
happy with my installation, so there is discussion at the end about next steps.
:::

::: details
[Why do you use so many package managers?]

I'm sick of being asked to choco yumbrew pip6 npm install every project on the
internet. I have a package manager already - let me use it.
:::

::: details
[Tips for debugging a new server's network connection]

I was trying to install discourse on a new server, and the installer kept
reporting issues with the network connection. This forced me to learn some tips
about debugging a network, which turned out not to fix the issue.
:::

::: details
[Accessible Section Linking]

I wrote a pandoc filter that put little anchors next to all my headers,
automatically. This is how!
:::

::: details
[Magic Spells]

A cheat-sheet for long commands with many options that I don't
like searching the internet or reading the manual to find over
and over.
:::


::: details
[Controlling CPU Frequency and Temperature in Gentoo]

When I switched to Gentoo, my laptop was warmer than I expected
during normal use. It turned out that my CPU was running at max
frequency all the time, which was making it consume more power
(and hence warm up).
:::


::: details
[Keyboard Settings under X]

Explanation of how to remap keys, assign scripts to certain key
combinations, and fix broken multimedia keys on a system running
X.
:::

::: details
[st - simple terminal][my-st]

st is a simple terminal emulator for X which [sucks less][st] It is
configured by patching the source code, which is written in C. To see
the changes I've made, plus an explanation of how they work, take a
look at [my fork][my-st] on Github.
:::


## Photos

I like to travel, and I take pictures when I travel. Here are a few of them:

[![Low Water, Coniston]](https://flic.kr/p/2mPdDz8){target="_blank"}
[![Coniston Water]](https://flic.kr/p/2mPeRAg){target="_blank"}
[![Hillside near Sawrey Knotts]](https://flic.kr/p/2mPajkQ){target="_blank"}
[![Wheat field, Croesgoch]](https://flic.kr/p/2mPbBKQ){target="_blank"}
[![Sunset, Croesgoch]](https://flic.kr/p/2mPeQTe){target="_blank"}

You can find more on [my flickr][flickr].

[low water, coniston]: https://live.staticflickr.com/65535/51730170949_b78b2e9687_c_d.jpg
[coniston water]: https://live.staticflickr.com/65535/51730406495_117f5e5134_c_d.jpg
[hillside near sawrey knotts]: https://live.staticflickr.com/65535/51729520926_4032b35279_c_d.jpg
[wheat field, croesgoch]: https://live.staticflickr.com/65535/51729774618_20bee0f97e_c_d.jpg
[sunset, croesgoch]: https://live.staticflickr.com/65535/51730404115_f719f7d075_c_d.jpg

[gentoo]: https://www.gentoo.org
[fsf]: https://www.fsf.org/
[markdown]: https://en.wikipedia.org/wiki/Markdown
[pandoc]: https://pandoc.org/
[st]: https://st.suckless.org/
[my-st]: https://github.com/danielittlewood0/st
[Magic Spells]: notes/magic-spells.html
[Controlling CPU Frequency and Temperature in Gentoo]: notes/cpu-and-temperature.html
[Keyboard Settings under X]: notes/keyboard.html
[Accessible Section Linking]: notes/section-linking.html
[Why do you use so many package managers?]: notes/package-managers.html
[Tips for debugging a new server's network connection]: notes/network-debugging.html
[Migrating to NixOS]: notes/migrating-to-nixos.html
[DeGoogle: Leaving Gmail]: notes/leaving-gmail.html

[flickr]: https://www.flickr.com/photos/194548325@N07/?fbclid=IwAR3Fr3G5z98du4JbVvcEEAkW5z7N-ISFaMqF74FCpfRHeoCj2zjxT-Z2qEI

[the documentation]: notes/website-documentation.html
[the source]: https://gitlab.com/danielittlewood0/danielittlewood-xyz
