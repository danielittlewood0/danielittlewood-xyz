# Bookmarks

I routinely have over a hundred tabs open. At the time of writing this, it's over 200. There are basically two reasons I have so many:

* I start a project and do some exploration but never finish.
* I see an article, think "that looks cool" but don't have time to read it right now.

So: this page is just an attempt to document what links I want to look at later, and why.

## Events I want to go to

* [Oggcamp 2024](https://ogg.camp/) is on Oct 12/13th, in Manchester.
* [FOSDEM 2025](https://fosdem.org/2024/news/2024-09-18-fosdem-2025-dates/) is on Feb 1/2, in Brussels.

## GNU Guile

Guile is a 
