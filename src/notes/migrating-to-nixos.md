---
title: "Migrating to NixOS"
date: "2 January 2023"
---

I have been meaning to try out NixOS for a long time, for a couple of reasons:

* Nix allows for easy isolation of build environments, similar to Docker. My
  hope is that by integrating more closely with the host system, it will be
  possible to work on projects conveniently without polluting the host.
* Since packages are isolated from the host, I think you should be able to use
  very very old versions of software without much trouble. This is handy for
  resurrecting old free software projects, where the dependencies are
  incompatible with the host system.
* NixOS reproduces your system configuration from a list of specific files,
  which can be easily shared. So it should be easy to keep different machines I
  use in sync.

## Getting started

The first step is to boot from a LiveCD. To create one, get a USB stick (4GB or greater should be enough), download an ISO from [nixos.org](https://nixos.org/download.html), and burn it to the USB:

```
# Note that this will destroy all data on the stick
dd if=/path/to/image.iso of=/dev/usb-stick bs=4M
```

Then power your laptop on, enter the boot menu (however you do that - on my laptop, you press Enter then F12) and boot from the USB. You should see a NixOS prompt.

You may be able to use the graphical installer - if so, be my guest. I wanted
to make my root filesystem BTRFS, which wasn't an option, so I had to follow
the manual installation instructions. The manual is available online
[here](https://nixos.org/manual/nixos/stable/), or you can run `nixos-help`
from the command line. The high level strategy is the following:

1. Set up partitions and filesystems.
2. Build a configuration.nix file in the root filesystem.
3. Build the system from your configuration file.

Steps 2 and 3 are repeated whenever you want to update the system (update configuration.nix, rebuild, repeat).

### Setting up partitions

This is the same as for any other Linux distribution. I am working from an existing system, which has:

* /dev/sda2, a FAT32 boot partition,
* /dev/sda3, a swap partition,
* /dev/sda4, an EXT4 root partition,
* /dev/sda6, an NTFS data partition

I plan to keep this structure around, and re-use the existing swap and boot partitions. I might add in the data partition later. So we just need a replacement root partition. The LiveCD includes gparted, so just create a partition with your favourite filesystem.

[Section 2.3](https://nixos.org/manual/nixos/stable/#sec-installation-manual-partitioning) covers this pretty well.  Once your partitions are formatted, run the following as root:

```
# mount the root fs
mount your-root-partition /mnt
mkdir -p /mnt/boot
mount your-boot-partition /mnt/boot
swapon your-swap-partition
```

### Generating the config

Now that the filesystem is mounted, we can generate the default config:

```
nixos-generate-config --root /mnt
```

At this point you have created a file at `/mnt/etc/nixos/configuration.nix`, which will be used to build the system when you run `nixos-install`. I recommend checking out Chapters 6-14 of the manual to figure out what you want to set the remaining config options to (particularly 6-7, which explain the general syntax of .nix files). Chapters beyond 14 are only really necessary if you want to run a NixOS-based server. I left my desktop environment as GNOME just to make sure nothing weird would happen.

Reboot, and if all has gone well you should be able to boot into NixOS.

## Channels

A channel is like a release branch in a normal distribution. As usual, there are versioned releases (in this case, 22.11) and an "unstable" branch which just follows the versions of packages as they're released. There's also a `*-small` version which tries to be half-way between (a small number of core packages are tested properly, and the rest are released as source).

Note that channels are set up both system-wide (under root) and per-user. They are stored in a file `~/.nix-channels`. So far in the installation, you should have one system-wide channel, for the release you built, and no home channels. To inspect them,

```
nix-channel --list

cat ~/.nix-channels
cat: /home/daniel/.nix-channels: No such file or directory
sudo nix-channel --list
nixos https://nixos.org/channels/nixos-22.05
sudo cat /root/.nix-channels
https://nixos.org/channels/nixos-22.05 nixos
```

Above, nixos is the alias for `https://nixos.org/channels/nixos-22.05`. To upgrade from 22.05 to 22.11 (latest stable),

```
# as root
nix-channel --remove nixos
nix-channel --add https://nixos.org/channels/nixos-22.11 nixos
nix-channel --update
nixos-rebuild switch
```

Depending on what was updated, you may or may not need to reboot. Any updated packages should be available immediately.

## Remaining steps

The steps above cover the basic installation of a NixOS system. There are some
remaining steps for migrating over from an existing system. I'll try to cover
those in future articles.

* How can I migrate over packages from my old OS?
* How do I copy over my old configuration, while still allowing Nix to control it?
* How can I deal with programs that depend on secret data, like ssh or a password manager?
* How can you replicate your nix configuration across multiple machines?
