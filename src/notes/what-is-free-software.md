---
title: What is Free Software?
date: 17 July 2024
---

The "free software movement" is a single-issue political movement beginning in
1986, with exactly one goal: "software freedom" for all software, and for all
users. They define software freedom in terms of "the four freedoms" ([no, not
those four freedoms](https://archive.org/details/Fdr-TheFourFreedoms6January1941)),
which sound like rights.

0. **Use** The right to use a program, for any purpose.
1. **Change** The right to modify that program, to do things it doesn't do already.
2. **Share** The right to distribute programs to people who need them.
3. **Share** The right to distribute your modifications to other people.

These are not unqualified rights. They operate within the laws and ethics of
the surrounding society: if something is illegal, or wrong, it is not a matter
of software freedom that you should be able to do it anyway. I think the reason
people call them freedoms is that they refer to the freedom not to be coerced
*by the software owner*. So freedom 0 is really the freedom from coercion by
the owner of the software to use the software to do something. It says nothing
about coercion from the state, except that that coercion requires justification.

In the modern world, none of these freedoms are guaranteed.
Some limitations on software freedom are the result of state action. Software
is covered by copyright in most of the world, which prohibits sharing and
modifying software except with the permission of the owner. Software patents
define certain algorithms or technical processes which, while publicly known,
may not be implemented by anyone - except by permission of the patent holder. 
And owners often require the user to agree to an "End User License Agreement"
(EULA) in order to use the software at all. The terms in an EULA can cover
anything at all, and the only way to definitively find out a term is
"unenforceable" is for a judge to say so in court, which is an expensive and
intimidating hurdle.

Other restrictions happen passively, and it would require state intervention to
prevent them. Most software comes to users not in the form it was written, but
"compiled" into instructions that the computer can interpret directly. If a
vendor distributes only this "binary" copy, nobody can force them to distribute
the "source" version that they used to create it. Changing a binary in a way
that results in a useful change to the program is very hard, often to the point
of being impossible.

It used to be the case that even if the software could not be
reverse-engineered, if someone wanted to do some equivalent task on the same
data, then they could implement the software themselves, from scratch. It was
later realised that the program could encrypt that data, using secret keys
known only to the software or hardware owner, and thereby "lock" the
information so that anyone who received a document in that format had no choice
but to buy a copy of the software. The so-called "Digital Rights Management"
(DRM) provides quite an extreme degree of power over the user: the owner could
make files that can be read one day and not another, or be read on one computer
but not another, or be read five times and then "self-destruct". Attempting to
circumvent such a restriction (to determine the keys, or to read the hidden
data) is illegal, and carries a multi-year jail sentence.

Another related trend in the software industry is "Software as a Service"
(SaaS), which performs a bait-and-switch: rather than being given any software
at all, the user is merely permitted to access a web server, which is where the
software actually runs. Since the server has total control over the software
running, the user is entirely at their whim: the software could stop working at
any time, for any reason (accidental or deliberate), with no "higher power"
that can force the vendor to restore access. Often in this case, the data is
also stored exclusively on the remote machine. After using such a system for
multiple years, the "network effect" prevents many users from ever even
considering changing vendor.

An advocate for Software Freedom in my sense would consider all of the
aforementioned mechanisms not just annoying or inconvenient, but *morally
wrong* in and of themselves. For instance, many people pay money in order to use
software, and if the price is not too high, then they don't see anything wrong
with that. They might complain if the price goes up, but they don't view
themselves as a victim of some grand injustice. And in fact, this is not
directly unjust. But in order to control the price of a copy, the owner must
have control over who can share the software. The owner is free to choose not
to sell the software to certain people at any price - after all, you can't
force a shop to sell you goods. If they don't want to deal with you, then tough
luck. Similarly with modifications: if you really want to add a feature to,
say, Microsoft Word, then they have the right to refuse to allow it to be
added. Not just refuse to add it, but refuse to *let you do the work to add it
yourself*.

Here, the advocate says, you have gone too far. Software is too enmeshed with
modern life to allow individuals such great control over how it is used.

Anyway, that's what the advocate believes. But *why should you* feel the same
way? I plan to write in more detail about this in the future.
