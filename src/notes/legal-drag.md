---
title: Legal Drag
date: 17 July 2024
---

# Where your rights went: how technology facilitates the erosion of rights in the cultural commons

I've been thinking a lot lately about fiscal drag. Fiscal drag is an economic
phenomenon where a person's income and expenses increase together (due to
inflation), but the thresholds for higher rates of tax remain the same. The
result is that, on top of any other economic issues they might be facing, a
greater proportion of their take home pay is taxed, and their spending power
decreases. In other words: their life gets worse.

In technology there is a similar phenomenon, which I want to call something
like "legal drag", whereby people lose certain rights and freedoms as a mere
consequence of technical change, and not because of any deliberate policy or
legislation. Well, not policy from the *government* in any case.

[A work locked up by DRM can be prevented from ever passing into the public
domain](http://www.terminally-incoherent.com/blog/2010/05/03/drm-death-of-public-domain/).
If the DRM is not broken (which is illegal), and the work is never released in
a physical medium (which is going out of style, because it isn't as
profitable), then the work could remain available forever while never allowing
anyone to make derivative works from it. In reality, the likely situation is
that the work will be destroyed long before the end of its useful life, [in order to
evade
taxes](https://jacobin.com/2023/07/disney-content-purge-streaming-worker-pay-tax-evasion),
or something else equally distopian. Obviously, this criticism applies equally
to "fair uses" of the material - while you have the legal right to make copies
of the work, there is no legal way to exercise that right.

[In the EU, someone who buys a copy of software has a right to sell that
copy](https://arstechnica.com/tech-policy/2016/10/software-backup-copies-cannot-be-re-sold-rules-cjeu/),
[even if it is communicated over a
network](https://www.clarionsolicitors.com/articles/reselling-used-software-licences-new-ruling-from-the-european-court).
They also have a right to [make backups of
software](https://eur-lex.europa.eu/EN/legal-content/summary/computer-programs-legal-protection.html).
However, if that software is distributed as a "service", then no communication
of the software happens, and the user has no rights at all. For example, if a
user pays for a "perpetual" license to use a SaaS, the company is under no
obligation to continue to provide the service.

More examples to follow...
