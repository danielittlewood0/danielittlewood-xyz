with import <nixpkgs> {};

# This file will be run whenever you call nix-shell in the top directory.
# To build, you should be able to run e.g.
# nix-shell --pure --run make

# Alternatively, just running nix-shell will give you an impure environment
# (text editor etc available) where you can just run make, make clean, etc as
# you like.

# I deliberately haven't specified exact versions because it *shouldn't matter*.

stdenv.mkDerivation {
  name = "danielittlewood-xyz";
  buildInputs = [
    pkgs.gnumake
    pkgs.pandoc
    pkgs.bc # required by sitemap.sh
    pkgs.rsync # required by upload.sh
    (pkgs.haskellPackages.ghcWithPackages (pkgs: with pkgs; [ pandoc pandoc-types ])) # required to build filters
  ];
}
