{-# LANGUAGE OverloadedStrings #-}
-- ^ this allows "string literals" to be type-inferred as, say, Text

-- details.hs
import Text.Pandoc.JSON
import System.Environment (getArgs)

{-
  This filter extends Markdown by adding syntax for the HTML5 details element.

  It consumes a Markdown input of the form
  ::: details
  Summary Text

  Main body text
  :::

  and produces markup
  <details>
  <summary> Summary Text </summary>
  Main body text
  </details>

  Where the inner content elements have been marked up as usual.
  The summary tag is assumed to be a single block element as defined by Pandoc.
  Any remaining blocks are hidden inside the disclosure element.

  When outputting to other formats, the content is simply rendered without a details element.

  For more information about <details>, see https://developer.mozilla.org/en-US/docs/Web/HTML/Element/details
-}

details :: Maybe Format -> Block -> [Block]
details (Just (Format "html")) original@(Div (_id, classes, _attributes) (summary:rest))
  | "details" `elem` classes = [ detailOpen ]
                            ++ [ summaryOpen, summary, summaryClose ] 
                            ++ rest
                            ++ [ detailClose ]
  | otherwise = [original]
    where [detailOpen, detailClose, 
           summaryOpen, summaryClose] = map (RawBlock "html") ["<details>", "</details>",
                                                               "<summary>", "</summary>"]
details _ original = [original]

main :: IO ()
main = toJSONFilter details
