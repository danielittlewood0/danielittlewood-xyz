{-# LANGUAGE OverloadedStrings #-}
-- ^ this allows "string literals" to be type-inferred as, say, Text

import Text.Pandoc.JSON
import System.Environment (getArgs)

{-
  This filter mimics functionality from e.g. the Cloudflare documentation,
  where every header in a page contains a link to itself. A user can hover over
  the link to inspect it, copy it to their clipboard, or click it to bring the
  section to the top of their view.

  It's appealing for the anchor tag to surround the header, for a few reasons:
  * a around h and not h around a, because it would be nice to extend the
    clickable area to the entire horizontal area taken up by the heading.
  * a around h rather than a *inside* h, because something like <a>#</a> would
    lack semantic content.
  * a around h rather than a *adjacent to* h, because without further
    assumptions on the layout there is no natural location to be clicked.
  * Finally, a around h translates more readily to e.g. LaTeX/PDF.

  Oh - but wrapping a Header in a Link is not type-allowed because Links are
  Inline with Inline children and Headers are Blocks with Inline children.

  My next favourite is adding the anchor as a child of the header.
  Hence, this will simply replace <h id="blah">content</h> by
  <h id="blah"><a href="#blah">#</a>content</h>.

  But that has annoying accessibility behaviour (link with no content) so I am
  now trying h around a.

  Phrasing this in terms of Pandoc elements ensures we don't have to do any
  case matching on the format.
-}

-- Time saved by copying https://frasertweedale.github.io/blog-fp/posts/2020-12-10-hakyll-section-links.html (CC-BY)
addLinksToHeaders :: Block -> Block
addLinksToHeaders (Header n attr@(id, _, _) children) | n > 1 = 
  let link = Link nullAttr children ("#" <> id, "")
  in Header n attr [link]
addLinksToHeaders x = x

main :: IO ()
main = toJSONFilter addLinksToHeaders

