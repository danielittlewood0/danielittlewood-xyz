This is the source code for [my personal site](https://www.danielittlewood.xyz). 
It is intended first and foremost to be *simple* - the idea is to get the
absolute best functionality out of the minimum "fancy tools". 

There are a couple of reasons behind this:
* Standardised HTML can be viewed and read by a variety of technologies,
  including accessibility tools like screen readers.
* Simple documents are more likely to be faithfully read by alternative
  browsers (i.e. not chrome or firefox), which encourages diversity in the
  client technology of the web.
* Less code makes the project easier to understand and adapt to new use cases.
  For example, someone could easily adapt this project to their own blog
  without needing to define a "this-particular-site-generator-api" that will box
  them in anyway.
* Client-side rendering is complex, heavy and buggy. Most of my worst user
  experiences on the web have been with sites that rely heavily on JS.
* Most sites that rely on client-side JS run proprietary code on the client's
  computer, which is unethical.
* Static content is much easier to cache, reducing the electrical impact of
  hosting your site.

At least at the time of writing, it relies on purely static content, with no JS at all.

If the document can be reasonably encoded in vanilla Markdown, then I will
write it in that. I reserve the right to extend Markdown using Pandoc, but all
.md documents are expected to render at least reasonably in any Markdown
renderer. Markdown extensions are preferable to using another format, because
they keep the source documents legible.

Styling is expected to be more or less global. I may add local overrides, but I
don't expect to. The site should be legible even with no styling at all.
The styles should be simple enough that they fit nicely into one file.

## Build

* The site is built using `make`. Read the `Makefile` for details about the
  general structure.
* The build-time dependencies are specified in `shell.nix`. Read that file if
  you want to understand it. You should be able to read it even if you don't
  know any Nix.
* The only runtime dependency is an HTTP server. I use nginx on my live site,
  but the design is expected to function more or less correctly with any
  server. Details of nginx-specific features are in the hidden `.htaccess` files.

## Run

After building, the `public` directory should contain all site content.
To run it locally,

```
python -m http.server 8000 --directory=public
```

will serve it on `localhost:8000`. But you can change the language if
python isn't your favourite.

## Upload

The upload script is very transparent: It uses
[rsync](https://linux.die.net/man/1/rsync) to send the build files over SSH.

## Caching

Browsers will very opportunistically cache static content. If developing
locally, you may need to do a refresh which bypasses the cache. See
[Wikipedia:Bypass your cache](https://en.wikipedia.org/wiki/Wikipedia:Bypass_your_cache)
for instructions.

I have never set up deliberate caching. Maybe I will at some point.
