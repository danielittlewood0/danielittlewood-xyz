# License

🄯 Daniel Littlewood

This work consists of a combination of software and non-software source files.
Insofar as those files are copyrightable, they are conveyed to you under either of the following licenses:

* The Creative Commons Attribution-ShareAlike 4.0 International License. To
  view a copy of this license, visit <http://creativecommons.org/licenses/by-sa/4.0/>,
  or look at the source file `CC-BY-SA-4.0.txt` in this repository.
* The GNU General Public License v3.0, or any later version. To view a copy of
  this license, visit <https://www.gnu.org/licenses/gpl-3.0-standalone.html>,
  or look at the source file `GPL-3.0.txt` in this repository.

For software, the latter choice is recommended. For non-software, the former is recommended.

`SPDX-License-Identifier: CC-BY-SA-4.0 OR GPL-3.0-or-later`

## Other works

This site is distributed with the following fonts, each distributed under the
SIL Open Font License. The full license is included in the site's source.

* Inconsolata, a monospace font for code by Raph Levien: https://fonts.google.com/specimen/Inconsolata
* Libre Baskerville by Impallari Type, a serif font based on a font by John Baskerville: https://fonts.google.com/specimen/Libre+Baskerville
